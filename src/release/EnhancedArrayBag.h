/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    EnhancedArrayBag.h
 * @authors Ryan Hall <ryan816@missouristate.edu>
 *          Connor Jansen<Jansen095@live.missouristate.edu>
 *			Taylor Kuttenkuler <tay124@live.missouristate.edu>
 * @brief   Header file for an array-based implementation of the EADT bag.
 *
 * @copyright 2017 Pearson Education, Hoboken, New Jersey.
 */
 
 #ifndef ENHANCED_ARRAY_BAG_
 #define ENHANCED_ARRAY_BAG_
 
 #include "ArrayBag.h"
 
 template<class ItemType>
 class EnhancedArrayBag : public ArrayBag<ItemType> {
	 public:
		 /*
         * @brief uses the toVector method to grab the contents of each bag and then
         * creates a new 'Bag' and fills it with the previously generated vectors
         * @param otherBag, the second 'Bag' who's contents are copied to new 'Bag'
         * @return newBag, a new 'Bag' containing exact copies of the two 'Bags'
         * passed to it.
         */
		virtual EnhancedArrayBag<ItemType> unionWithBag(const ArrayBag<ItemType>& otherBag);
		
		/*
         * @brief creates a new bag containing entries that are present in the bag
         * recieving the call and the bag passed as the single argument
         * @param otherBag, the other bag used to find the intersection between
         * two bags.
         * @post Both bags used from the call to the function remain unchanged
         * @return a new bag containing entries located in both bags passed to the
         * function
         */
		virtual EnhancedArrayBag<ItemType> intersectionWithBag(const ArrayBag<ItemType>& otherBag);
		
		 /*
         * @brief gets the frequency of each entry in the Bag recieving the
         * call ot the method and subtracts that from the frequency of the same
         * entry in the Bag passed as the single argument. It then puts that many
         * of an entry into a new bag
         * @param otherBag, the other bag whose difference is taken from.
         * @post the Bag that recieves the call to the method and the Bag
         * passed as an argument remain unchanged.
         * @return a new bag that contains the differnce from the bag that recieves
         * the call from the Bag passed as an argument
         */
		virtual EnhancedArrayBag<ItemType> differenceWithBag(const ArrayBag<ItemType>& otherBag);
 }; // end Enhanced Array Bag
 
 #include "EnhancedArrayBag.cpp"
 
 #endif