/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file    EnhancedArrayBag.cpp
 * @authors Ryan Hall <ryan816@missouristate.edu>
 *          Connor Jansen<Jansen095@live.missouristate.edu>
 *			Taylor Kuttenkuler <tay124@live.missouristate.edu>
 * @brief   Implemntation file for an array-based implementation of the EADT bag.
 *
 * @copyright 2017 Pearson Education, Hoboken, New Jersey.
 */
 
 #include "EnhancedArrayBag.h"
 #include <cstddef>
 
 template<typename ItemType>
 EnhancedArrayBag<ItemType> EnhancedArrayBag<ItemType>::unionWithBag(const ArrayBag<ItemType>& otherBag) {
	 EnhancedArrayBag<ItemType> unionBag;
	 
	 std::vector<ItemType> bag1Content = this->toVector();
	 std::vector<ItemType> otherBagContent = otherBag.toVector();   //creates vectors to hold contents of original Bags
	 
	 for (ItemType element : bag1Content){    //iterates through elements in first Bag
            unionBag.add(element);        //adds those element to the unionBag
          }
     for (ItemType element : otherBagContent){  //iterates through elements in second Bag
            unionBag.add(element);        //adds those element to the unionBag
          }
     return unionBag;
 }
	 
 template<typename ItemType>
 EnhancedArrayBag<ItemType> EnhancedArrayBag<ItemType>::intersectionWithBag(const ArrayBag<ItemType>& otherBag) {
	 EnhancedArrayBag<ItemType> intersectBag;
	 EnhancedArrayBag<ItemType> unionBag = unionWithBag(otherBag);
	 
	 std::vector<ItemType> bag1Content = this->toVector();
	 std::vector<ItemType> otherBagContent = otherBag.toVector();   //creates vectors to hold contents of original Bags

         for (ItemType element : bag1Content){        //Iterates through each item in first Bag

			if (intersectBag.getFrequencyOf(element) < otherBag.getFrequencyOf(element)){   //if the element is in the otherBag more than the intersectionBag
                intersectBag.add(element);      //Adds the element to the intersectionBag
             }
         }
	 return intersectBag;
 }
 
 template<typename ItemType>
 EnhancedArrayBag<ItemType> EnhancedArrayBag<ItemType>::differenceWithBag(const ArrayBag<ItemType>& otherBag) {
	 EnhancedArrayBag<ItemType> differenceBag;
	 
	 std::vector<ItemType> bag1Content = this->toVector();
	 std::vector<ItemType> otherBagContent = otherBag.toVector();
	 
	 for (ItemType element : bag1Content){
		 
		 int bag1Freq = this->getFrequencyOf(element);
		 int bag2Freq = otherBag.getFrequencyOf(element);
		 signed int count = bag1Freq - bag2Freq;
		 while (count > 0){
			 differenceBag.add(element);
			 count--;
		 }
	 }
	
	 return differenceBag;
 }
 
	 
